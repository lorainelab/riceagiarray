#!/bin/bash

# change this for your system
H=$HOME/Dropbox/RiceAgiArray
M=$H/data/O_sativa_japonica_Oct_2011.bed.gz
M1=$H/results/bedtools/annots.bed
B=$H/results/tophat/accepted_hits.bam
B1=$H/results/bedtools/44k.bed
OL=$H/results/bedtools/overlap.bed
# should use this for gene models - probes that map to introns
# won't detect the gene:
# -splitTreat "split" BAM or BED12 entries as distinct BED intervals.

# should use this for probe alignments
# the entire probe should map onto a gene model for it to be counted
# as a target
# -rRequire that the fraction overlap be reciprocal for A and B.
# - In other words, if -f is 0.90 and -r is used, this requires
#   that B overlap 90% of A and A _also_ overlaps 90% of B.

# http://bedtools.readthedocs.org/en/latest/content/tools/intersect.html

#gunzip -c $M | cut -f 1-12 | bedtools intersect -abam $B -b $M -loj -wa -bed -split -f 1

# convert BAM to bed
#bedtools bamtobed -i $B -ed > $B1   
#gunzip -c $M | cut -f1-12 > $M1
bedtools intersect -a $B1 -b $M1 -f 1 -wa -wb > $OL