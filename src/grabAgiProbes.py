#!/usr/bin/env python
"""
Used this to grab ids and sequences from XML file from Agilent.
First ran this command:
grep -A 1 "reporter name"  015241_D_F_20131129.xml > grepped.txt

If you need to modify or expand this, ask Ann first.

Also, this script is a total one-off and makes scary assumptions
about the XML file format. 
"""
import sys

def main():
    fh = open('grepped.txt')
    name=None
    feature_number=None
    sequence=None
    while 1:
        line=fh.readline()
        if not line:
            break
        if line.startswith('--'):
            write_vals(name,feature_number,sequence)
            continue
        line = line.strip()
        if line.startswith('<reporter name'):
            name=getName(line)
            sequence=getSequence(line)
            continue
        if line.startswith('<feature number'):
            feature_number=getFeatureNumber(line)
            continue
    write_vals(name,feature_number,sequence)

def write_vals(name,feature_number,sequence):
    # sys.stdout.write('\t'.join([name,feature_number,sequence])+'\n')
    if not sequence=='NA':
        sys.stdout.write('>%s %s\n%s\n'%(feature_number,name,sequence))
    
def getName(line):
    return line.split('"')[1]

def getSequence(line):
    toks=line.split('"')
    i = 0
    for tok in toks:
        if tok.strip()=='active_sequence=':
            return toks[i+1]
        i=i+1
    return 'NA'

def getFeatureNumber(line):
    return line.split('"')[1]

if __name__ == '__main__':
    main()
