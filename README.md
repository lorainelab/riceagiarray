# Background

This repo contains files related to mapping Agilent rice 44K
array probes onto the MSU7 release of the rice genome annotations.
We need this so that we can compare 44K expression array results
to RNA-Seq data.

# Methods

* Extract probe ids and sequences from XML file provided by Agilent.
* Map probes onto rice genome assembly using tophat spliced aligner.
* Use bedtools to identify probe alignments that intersect with rice gene models.

## Data files used included

* probes XML file 015241_D_F_20131129.xml.gz from Agilent
* rice genome assembly from Oct 2011 (O_sativa_japonica_Oct_2011.2bit)
* MSU7 gene models in BED-detail format (O_sativa_japonica_Oct_2011.bed.gz)


Rice data files were from [genomes repository](http://svn.transvar.org/repos/genomes/trunk), 
revision 989. 

The BED file is in BED-detail format, sorted, and bgzip compressed so that
it can be used with tabix. 

To to use it with bedtools remove the last two columns using cut.
